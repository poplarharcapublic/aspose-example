﻿using Microsoft.AspNetCore.Http;
using Aspose.Example.Models.Reporting.Document;
using Aspose.Example.Models.Reporting.Transaction;
using Aspose.Example.Models;

namespace Aspose.Example.Services
{
    public class AccountsReportingService
    {
        private string subaccountDesc;
        private int accountIDFinal;
        private int offsetLeaseHolder = 2000000;

        public AccountsReportingService()
        {

        }

        public async Task<FileResult> GeneratePDF()
        {
            DateTime dateFrom = DateTime.Parse("01/01/2015 00:00:00");
            DateTime dateTo = DateTime.Parse("12/12/2023 23:59:59");

            int accountID = 2000139;
            int personID = 25631;
            int? subAccount = 0;

            var reqDoc = new AccountReportDocument();
            List<string> templateOptionList = new List<string> { "ServiceCharges" + Path.DirectorySeparatorChar + "MainPage.htm" };

            List<tenancy> tenancyPersInf = Data.TenancyData;
            List<summary> SubAcctSummaries = Data.SubAccountData;
            List<summary> subaccountSearch = SubAcctSummaries.FindAll(e => e.acctID == accountID);
            List<summary> subAccFinal = subaccountSearch.FindAll(e => e.per_num == personID);
            List<tenancy> listTen = tenancyPersInf.FindAll(e => e.tenancy_id == accountID);
            List<tenancyPerson> tenancePeople = listTen[0].tenancyperson;
            List<tenancyPerson> tenancyPerson = tenancePeople.FindAll(e => e.personid == personID);
            List<transaction> transactions = Data.Transactions;

            transactions = transactions.OrderBy(t => t.RowNum).ToList();

            List<Display> transPDFTable = transactions.Select((singleTrans) =>
                {
                    var singleRowPDF = new Display()
                    {
                        AccountNumber = singleTrans.accountid.ToString(),
                        Number = singleTrans.invoice_number.ToString(),
                        DateTime = (DateTime)singleTrans.date,
                        Description = singleTrans.dsc,
                        Balance = singleTrans.runningtotal
                    };
                    if (singleTrans.val < 0)
                    {
                        singleRowPDF.Payments = -singleTrans.val;
                    }
                    else
                    {
                        singleRowPDF.Charges = singleTrans.val;
                    }
                    return singleRowPDF;
                }
            ).ToList();

            var lastTransaction = transactions.MaxBy(x => x.RowNum);
            var firstTransaction = transactions.MinBy(x => x.RowNum);

            reqDoc.BalanceBroughtForward = firstTransaction.runningtotal - firstTransaction.val;
            reqDoc.BalanceCarriedForward = lastTransaction.runningtotal;
            reqDoc.Amount = Math.Abs(reqDoc.BalanceCarriedForward);

            if (reqDoc.BalanceCarriedForward <= 0)
            {
                reqDoc.Direction = true;
            }
            else
            {
                reqDoc.Direction = false;
            }

            if (subAccount == null)
            {
                reqDoc.AccountType = "Consolidated";
            }
            else
            {
                List<summary> subAccDescGrab = subAccFinal.FindAll(e => e.subAcct == subAccount);

                reqDoc.AccountType = subAccDescGrab[0].dsc;
                string subDesc = subAccDescGrab[0].dsc.Replace(" ", "");
                this.subaccountDesc = subDesc;
            }

            var leftAddress = new Aspose.Example.Models.Reporting.Person.Address();
            var rightAddress = new Aspose.Example.Models.Reporting.Person.Address();

            if (listTen[0].correspondence_address.Count > 0)
            {
                var correspondenceAddress = listTen[0].correspondence_address[0];
                leftAddress.Address1 = correspondenceAddress.addr1;
                leftAddress.Address2 = correspondenceAddress.addr2;
                leftAddress.Address3 = correspondenceAddress.addr3;
                leftAddress.Address4 = correspondenceAddress.addr4;
                leftAddress.PostCode = correspondenceAddress.postcode;

                rightAddress.Address1 = listTen[0].addr1;
                rightAddress.Address2 = listTen[0].addr2;
                rightAddress.Address3 = listTen[0].addr3;
                rightAddress.Address4 = listTen[0].addr4;
                rightAddress.PostCode = listTen[0].postcode;
                reqDoc.AddressRightValue = rightAddress;
                reqDoc.hasCorrAddress = true;

            }
            else
            {
                leftAddress.Address1 = listTen[0].addr1;
                leftAddress.Address2 = listTen[0].addr2;
                leftAddress.Address3 = listTen[0].addr3;
                leftAddress.Address4 = listTen[0].addr4;
                leftAddress.PostCode = listTen[0].postcode;
            }

            reqDoc.AccountNumber = accountID;
            reqDoc.hasPropAddress = true;
            reqDoc.AddressLeftValue = leftAddress;
            reqDoc.StartDate = dateFrom;
            reqDoc.EndDate = dateTo;
            reqDoc.FullName = tenancyPerson[0].personame;
            reqDoc.Templates = templateOptionList;
            reqDoc.Transactions = transPDFTable;
            this.accountIDFinal = accountID;

            if (accountID > offsetLeaseHolder)
            {
                templateOptionList.Add("ServiceCharges" + Path.DirectorySeparatorChar + "BackPage.htm");
            }
            else
            {
                templateOptionList.Add("Rents" + Path.DirectorySeparatorChar + "BackPage.htm");
            }

            var asposePDFGen = new PDF.Statement.GenerateAccountsPDF(reqDoc);

            using MemoryStream pdfAsMemStream = asposePDFGen.Generate();

            return new FileResult()
            {
                FileName = $"Statement_{accountID}_{subaccountDesc}.pdf",
                ContentType = "application/pdf",
                Contents = pdfAsMemStream.ToArray()
        };
        }
    }
}
