﻿using Aspose.Pdf;
using Aspose.Example.Models.Reporting;
using Aspose.Example.Models.Reporting.Document;
using Aspose.Example.Models.Reporting.Transaction;

namespace PDF.Statement
{
    public class GenerateAccountsPDF
    {

        private string AssetsDirectory;
        private string TemplateDirectory;
        private int leaseHolderOffset = 5000000;

        AccountReportDocument Document;
        public static void SaveMemoryStream(MemoryStream ms, string FileName)
        {
            FileStream outStream = File.OpenWrite(FileName);
            ms.WriteTo(outStream);
            outStream.Flush();
            outStream.Close();
        }
        public GenerateAccountsPDF(AccountReportDocument document)
        {

            AssetsDirectory = System.IO.Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "PDFAssets" + Path.DirectorySeparatorChar;
            TemplateDirectory = System.IO.Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "PDFAssets" + Path.DirectorySeparatorChar + "Templates" + Path.DirectorySeparatorChar;
            Document = document;

        }
        public MemoryStream Generate()
        {
            adjustTemplate();
            Aspose.Pdf.License lic = new Aspose.Pdf.License();
            lic.SetLicense("Aspose.PDF.NET.lic");

            Document doc = new Document();
            MarginInfo marginInfo = new MarginInfo(Utilities.InCM(1.5), Utilities.InCM(5), Utilities.InCM(1.5), Utilities.InCM(3.5));
            doc.PageInfo.Margin = marginInfo;

            foreach (string template in Document.Templates)
            {
                Page section = doc.Pages.Add();
                AddPage(section, template);
            }
            
            doc.ProcessParagraphs();
            
            using (FileStream stream = File.OpenRead(Path.Combine(AssetsDirectory, "Images", "pdf_background.png")))
            {
                foreach (Page page in doc.Pages)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    BackgroundArtifact bga = new BackgroundArtifact();
                    bga.BackgroundImage = stream;
                    bga.ArtifactVerticalAlignment = VerticalAlignment.Center;
                    bga.ArtifactHorizontalAlignment = HorizontalAlignment.Center;
                    page.Artifacts.Add(bga);

                }

                MemoryStream output = new MemoryStream();
                
                doc.Save(output, Aspose.Pdf.SaveFormat.Pdf);
                doc.FreeMemory();
                doc.Dispose();
                return output;
            }

        }

        private void AddPage(Page page, string template)
        {

            StreamReader r = File.OpenText(TemplateDirectory + template);
            string html = r.ReadToEnd();
            string htmlText = FillHTML(html);
            HtmlFragment mainHTML = new HtmlFragment(htmlText);
            page.Paragraphs.Add(mainHTML);
            int pageNum = page.Number;
        }

        private void adjustTemplate()
        {

            if (Document.AccountNumber > leaseHolderOffset)
            {

                Document.DepartmentType = "Private Tenures";
                Document.StatementTypeHeading = "SERVICE CHARGE";
                Document.AccDescription = "Owner";

                Document.PeriodSummaryParagraph = String.Format("Here is your service charge statement for the period {0} " +
                    "to {1}. Please contact us if you have any questions or this the statement is not right.", Document.StartDate.ToString("dd/MM/yyyy"), Document.EndDate.ToString("dd/MM/yyyy"));
            }
            else
            {
                Document.DepartmentType = "Rents";
                Document.StatementTypeHeading = "RENT";
                Document.AccDescription = "Tenancy";

                Document.PeriodSummaryParagraph = String.Format("Here is your rent statement for the period {0} to {1} for {2}. " +
                    "If you get Housing Benefit / Universal Credit or pay monthly there may be arrears showing because your regular payment had not reached your account when this statement was printed. " +
                    "Please contact us if you think the statement is not right.", Document.StartDate.ToString("dd/MM/yyyy"), Document.EndDate.ToString("dd/MM/yyyy"), Document.AddressLeftValue.Address1);

            }
            if (Document.hasCorrAddress)
            {
                Document.AddressLeftTitle = "Correspondence Address";
                Document.AddressRightTitle = "Property Address";
            }
            else
            {
                Document.AddressLeftTitle = "Property Address";
                Document.AddressRightTitle = "";
            }

        }

        private string FillHTML(string html)
        {

            //html = html.Replace("[{PROPERTYADDRESS}]", Document.PropertyAddress.ToString());
            html = html.Replace("[{DEPTYPE}]", Document.DepartmentType.ToString());
            html = html.Replace("[{SUBJECTLETTERHEADING}]", Document.StatementTypeHeading.ToString());
            html = html.Replace("[{PERIODSUMMARYPARAGRAPH}]", Document.PeriodSummaryParagraph.ToString());
            //html = html.Replace("[{DIRECTDEBITPARAGRAPH}]", Document.DirectDebitParagraph.ToString());

            html = html.Replace("[{ADDLEFTTITLE}]", Document.AddressLeftTitle.ToString());
            html = html.Replace("[{ADDLEFTVALUE}]", Document.AddressLeftValue.ToString());

            if (Document.hasCorrAddress)
            {
                html = html.Replace("[{ADDRIGHTTITLE}]", Document.AddressRightTitle.ToString());
                html = html.Replace("[{ADDRIGHTVALUE}]", Document.AddressRightValue.ToString());
            }
            else
            {
                html = html.Replace("[{ADDRIGHTTITLE}]", "");
                html = html.Replace("[{ADDRIGHTVALUE}]", "");
            }

            html = html.Replace("[{ACCOUNTNUMBER}]", Document.AccountNumber.ToString());
            html = html.Replace("[{ACCOUNTTYPE}]", Document.AccountType);
            html = html.Replace("[{ACCDESC}]", Document.AccDescription);
            html = html.Replace("[{DATE}]", DateTime.Now.ToString("dd/MM/yyyy"));
            html = html.Replace("[{AMMOUNT}]", Math.Abs(Document.Amount).ToString("0.00"));
            html = html.Replace("[{DIRECTION}]", ((Document.Direction) ? "credit" : "arrears"));
            html = html.Replace("[{FULLNAME}]", Document.FullName);
            /*            html = html.Replace("[{STARTDATE}]", Document.StartDate.ToString("dd/MM/yyyy"));
                        html = html.Replace("[{ENDDATE}]", Document.EndDate.ToString("dd/MM/yyyy"));*/

            html = html.Replace("[{BALANCEBROUGHTFORWARD}]", Document.BalanceBroughtForward.ToString("0.00"));
            html = html.Replace("[{BALANCECARRIEDFORWARD}]", Document.BalanceCarriedForward.ToString("0.00"));

            html = html.Replace("[{TABLEDATA}]", BuildServiceChargeTableData());
            return html;
        }

        private string BuildServiceChargeTableData()
        {
            string table = "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;border-collapse: collapse;\">";

            string header = "<tr>";
            header += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;\" ><strong>Transaction Date</strong></td>";
            header += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;\"><strong>Transaction Number</strong></td>";
            header += "<td style=\"border: 1px solid black;padding: 0px 5px;\"><strong>Description</strong></td>";
            header += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;\"><strong>Charges (£)</strong></td>";
            header += "<td style=\"width:65px;border: 1px solid black;padding: 0px 5px;\"><strong>Paid (£)</strong></td>";
            header += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;\"><strong>Running Balance (£)</strong></td>";
            header += "</tr>";

            table += header;

            foreach (var d in Document.Transactions)
            {
                string row = "<tr>";
                row += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;font-size:11px;\" >" + d.DateTime.ToString("dd MMM yy").ToUpper() + "</td>";
                row += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;font-size:11px;text-align:center;\">" + d.Number + "</td>";
                row += "<td style=\"border: 1px solid black;padding: 0px  5px;font-size:11px;\">" + d.Description + "</td>";
                row += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;font-size:11px;text-align:right;\">" + ((d.Charges != 0) ? d.Charges.ToString("0.00") : "") + "</td>";
                row += "<td style=\"width:65px;border: 1px solid black;padding: 0px 5px;font-size:11px;text-align:right;\">" + ((d.Payments != 0) ? d.Payments.ToString("0.00") : "") + "</td>";
                row += "<td style=\"width:85px;border: 1px solid black;padding: 0px 5px;font-size:11px;text-align:right;\">" + d.Balance.ToString("0.00") + "</td>";
                row += "</tr>";

                table += row;
            }
            table += "</table>";
            return table;
        }

    }
}
