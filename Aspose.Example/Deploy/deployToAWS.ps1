﻿
$cluster = "dev"
$service = "aspose-example-service"
$task_definition = "ph_aspose-example_taskdefinition:5"
$targetGroupArn = "arn:aws:elasticloadbalancing:eu-west-2:084585804012:targetgroup/AposeAnand/c335f51cea274408"
$containername= "asposeexample"

$dockertag = "asposeexample:latest"
$password = aws ecr get-login-password --region eu-west-2
docker login --username AWS --password $password "084585804012.dkr.ecr.eu-west-2.amazonaws.com"


#list and stop existing tasks in AWS
echo "Delete running task"
$aws_tasks = aws ecs list-tasks --cluster dev --service-name $service | ConvertFrom-Json 
$task_arn = $aws_tasks.taskArns
$arrTaskArn=$task_arn.Split("/")
echo $task_arn
echo $arrTaskArn[2]
aws ecs stop-task --cluster $cluster --task $arrTaskArn[2]  | ConvertFrom-Json

##force delete the service
aws ecs delete-service --cluster $cluster --service $service --force | ConvertFrom-Json

#login to aws Repository
echo "log into aws Repository"
aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 084585804012.dkr.ecr.eu-west-2.amazonaws.com

#build the docker and push it
cd ..
docker build -t $dockertag .

write-host "Pushing to repository $dockertag"
docker tag $dockertag "084585804012.dkr.ecr.eu-west-2.amazonaws.com/$dockertag"

docker push "084585804012.dkr.ecr.eu-west-2.amazonaws.com/$dockertag"


$awsTaskStatus =""
while($awsTaskStatus -ne "Inactive")
    {
        ##check if the service is running - if it is not runnning an error will be returned
        $awsresponse = aws ecs describe-services --services $service --cluster $cluster

        $awsResponseJson = $awsresponse | ConvertFrom-Json
        ##State the task status - Active means task is up
        $awsTaskStatus = $awsResponseJson.services.status
        ##State the service failure status
        $awsTaskMissing = $awsResponseJson.failures.reason
        
        #wait 10 seconds before next try
        $b = Get-Date
        Write-Output "$b - Waiting for 10 seconds before retrying - service $service has the status:$awsTaskStatus"
        Start-Sleep -s 10
	}

#Create Service
echo "creating service in AWS"
aws ecs create-service `
 --cluster $cluster `
 --service-name  $service `
 --task-definition $task_definition  `
 --desired-count 1 `
 --launch-type "FARGATE" `
 --network-configuration "awsvpcConfiguration={subnets=[subnet-0c93bea94e37230bb],securityGroups=[sg-04dc81a6db541ff77],assignPublicIp=ENABLED}"  `  `
 --load-balancers targetGroupArn=$targetGroupArn,containerName=$containername,containerPort=80 | ConvertFrom-Json
