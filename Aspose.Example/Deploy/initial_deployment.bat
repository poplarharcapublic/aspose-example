﻿REM *********************************************
REM *		Build docker image		*
REM *********************************************
cd ..\aspose-example\Aspose.Example
docker build -t  asposeexample:latest .

REM *********************************************
REM *	Login to AWS docker repositories	*
REM *********************************************
aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 084585804012.dkr.ecr.eu-west-2.amazonaws.com

REM *********************************************
REM *		Create Repository in AWS	*
REM *********************************************
aws ecr create-repository ^
    --repository-name asposeexample ^
    --image-scanning-configuration scanOnPush=true ^
    --region eu-west-2

REM *********************************************
REM *	Prep image to be uploaded to AWS 	*
REM *********************************************
docker tag asposeexample:latest 084585804012.dkr.ecr.eu-west-2.amazonaws.com/asposeexample:latest

REM *********************************************
REM *	Push image to AWS Repository 		*I
REM *********************************************
docker push 084585804012.dkr.ecr.eu-west-2.amazonaws.com/asposeexample:latest

REM *********************************************
REM *		Define test task           			*
REM *********************************************
cd ..\aspose-example\Aspose.Example\Deploy\task
aws ecs register-task-definition --cli-input-json file://fargatetask.json

REM *********************************************
REM *           CREATE Fargate cluster          *
REM *********************************************

aws ecs create-cluster --cluster-name dev

REM *********************************************
REM *	 	Create Test Target Group 	     	*
REM *********************************************

aws elbv2 create-target-group ^
    --name AposeAnand ^
    --protocol HTTP ^
    --port 80 ^
    --target-type ip ^
    --vpc-id vpc-044580902afb3a066

REM *********************************************
REM *		 Create test Service	     		*
REM *********************************************
 aws ecs create-service ^
 --cluster orchardAPIs ^
 --service-name  aspose-example-service ^
 --task-definition  ph_aspose-example_taskdefinition:1 ^
 --desired-count 1 ^
 --launch-type "FARGATE" ^
 --network-configuration "awsvpcConfiguration={subnets=[subnet-0c93bea94e37230bb],securityGroups=[sg-04dc81a6db541ff77],assignPublicIp=ENABLED}"  ^
 --load-balancers targetGroupArn=arn:aws:elasticloadbalancing:eu-west-2:084585804012:targetgroup/AposeAnand/c335f51cea274408,containerName=asposeexample,containerPort=80 
