﻿using System;
using System.Collections.Generic;

namespace Aspose.Example.Models
{
    public class tenancy
    {
        public int? tenancy_id { get; set; }
        public int? property_id { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public string property_name { get; set; }
        public string block_name { get; set; }
        public int? blockid { get; set; }
        public string addr1 { get; set; }
        public string addr2 { get; set; }
        public string addr3 { get; set; }
        public string addr4 { get; set; }
        public string postcode { get; set; }
        public int? property_group { get; set; }
        public string ARREAREA { get; set; }
        public string property_class { get; set; }
        public string property_type { get; set; }
        public List<tenancyPerson> tenancyperson { get; set; }
        public List<transaction> transactions { get; set; }
        public List<CorrespondenceAddress> correspondence_address { get; set; }
    }
}
