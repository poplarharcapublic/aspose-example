﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspose.Example.Models.Reporting.Transaction
{
    public class BreakDown
    {
        public Guid BreakDownID { get; set; }
        public string PropertyID { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public decimal EstimatedCost { get; set; }
        public decimal ActualCost { get; set; }
        public DateTime DateTime { get; set; }

        public string EstimatedCostDisplay
        {
            get
            {
                return EstimatedCost.ToString("0.00");
            }
        }
        public string ActualCostDisplay
        {
            get
            {
                return ActualCost.ToString("0.00");
            }
        }
    }
}
