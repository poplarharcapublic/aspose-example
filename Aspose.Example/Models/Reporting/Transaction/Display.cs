﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspose.Example.Models.Reporting.Transaction
{
    public class Display
    {
        public decimal Charges { get; set; }
        public decimal Payments { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Balance { get; set; }
        public decimal PreviousBalance { get; set; }

        public string AccountNumber { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
    }
}
