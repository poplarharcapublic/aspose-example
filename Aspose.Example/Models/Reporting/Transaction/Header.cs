﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspose.Example.Models.Reporting.Transaction
{
    public class Header
    {
        public string TransactionID { get; set; }
        public string TenancyID { get; set; }
        public string OwnerAccountID { get; set; }
        public string PropertyID { get; set; }
        public int SubAccountID { get; set; }
        //public Transaction_Type Type { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public DateTime DateTime { get; set; }
        public string Number { get; set; }
        public string DateTimeDisplay
        {
            get
            {
                return DateTime.ToString("dd/MM/yyyy");
            }
        }
        public int? Year { get; set; }
        public bool? IsEstimateOrActual { get; set; }
        //public InvoiceType? InvoiceType { get; set; }
        /*        public string CreditDisplay
                {
                    get
                    {
                        return (Type == Transaction_Type.Credit) ? Amount.ToString("c") : "";
                    }
                }*/
        /*        public string DebitDisplay
                {
                    get
                    {
                        return (Type == Transaction_Type.Debit) ? Amount.ToString("c") : "";
                    }
                }*/
        public string BalanceDisplay
        {
            get
            {
                return Balance.ToString("c");
            }
        }
        /*        public string DirectionDisplay
                {
                    get
                    {
                        return (Type == Transaction_Type.Credit) ? "Credit" : ((Type == Transaction_Type.Debit) ? "Debit" : "");
                    }
                }*/
        /*        public Tenancy.Header Tenancy { get; set; }
                public OwnerAccount.Header OwnerAccount { get; set; }
                public Tenancy.SubAccount TenancySubAccount { get; set; }
                public OwnerAccount.SubAccount OwnerAccountSubAccount { get; set; }*/

    }
}
