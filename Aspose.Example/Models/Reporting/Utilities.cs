﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspose.Example.Models.Reporting
{
    public class Utilities
    {
        public static float InCM(double cm)
        {
            return (float)(cm * 28.3);
        }
    }
}
