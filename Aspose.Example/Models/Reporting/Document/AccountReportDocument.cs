﻿namespace Aspose.Example.Models.Reporting.Document
{
    public class AccountReportDocument
    {
        public List<string> Templates { get; set; }

        public List<Transaction.Display> Transactions { get; set; }

        public decimal BalanceBroughtForward { get; set; }

        public decimal BalanceCarriedForward { get; set; }

        public decimal Amount { get; set; }

        public bool Direction { get; set; }

        public int SubAccountID { get; set; }

        public string FullName { get; set; }

        public long AccountNumber { get; set; }

        public string AccountType { get; set; }

        public string AccDescription { get; set; }

        public string AddressLeftTitle { get; set; }

        public Person.Address AddressLeftValue { get; set; }

        public string AddressRightTitle { get; set; }

        public Person.Address AddressRightValue { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string DepartmentType { get; set; }

        public string StatementTypeHeading { get; set; }

        public bool hasCorrAddress { get; set; }

        public bool hasPropAddress { get; set; }

        public string PeriodSummaryParagraph { get; set; }

        public string DirectDebitParagraph { get; set; }
    }
}
