﻿namespace Aspose.Example.Models.Reporting
{
    public class Enumerations
    {

        public enum Question_Type : int
        {
            True_or_False = 1,
            Text = 2,
            Multiple_Choice = 3,
            Scale = 4,
            Unknown = 5,
            Single_Choice = 6,
            Multiple_Choice_Override = 7,
            Number = 8,
            Statement = 9,
            Date = 10,
            Phone_Number = 11,
            Email = 12,
            Money = 13,
            Property_Selector = 14,
            Checkbox = 21,
            Contacts_Confirmation = 31,
            Data_Protection_Statement = 32,
            Property_Selector_Multiple_Choice = 41,
            Digital_Signature = 42,
            Shipping_Address = 43,
            Document = 34,
            Text_Area = 45,
            Video = 44
        }

    }
}
