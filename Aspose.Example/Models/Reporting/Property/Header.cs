﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspose.Example.Models.Reporting.Property
{
    public class Header
    {
        public string PropertyID { get; set; }
        public string PropertyType { get; set; }
        public int PropertyTypeOrder
        {
            get
            {
                if (PropertyType == "House")
                {
                    return 1;
                }
                else if (PropertyType == "Flat")
                {
                    return 2;
                }
                return 3;
            }
        }
        public bool MaintenanceResponsibility { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string PostCode { get; set; }
        public string BlockID { get; set; }
        public string AreaID { get; set; }


        public string PropertyAddress
        {
            get
            {
                return string.Format("{0}<br>{1}<br>{2}<br>{3}<br>{4}", Address1, Address2, Address3, Address4, PostCode);
            }
        }
    }
}
