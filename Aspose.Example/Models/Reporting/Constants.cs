﻿using System.Text;

namespace Aspose.Example.Models.Reporting
{
    public class Constants
    {

        public const int BUFFERSIZE = 1048576;

        public const string NEWLINE = "\r\n";
        public const string HTMLNEWLINE = "<br />";

    }
}
