﻿namespace Aspose.Example.Models.Reporting.Person
{
    public class Address
    {
        public string PersonID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string PostCode { get; set; }
        public override string ToString()
        {
            if (!string.IsNullOrWhiteSpace(Address2))
            {
                Address2 = "<br>" + Address2;
            }

            if (!string.IsNullOrWhiteSpace(Address3))
            {
                Address3 = "<br>" + Address3;
            }

            if (!string.IsNullOrWhiteSpace(Address4))
            {
                Address4 = "<br>" + Address4;
            }

            if (!string.IsNullOrWhiteSpace(PostCode))
            {
                PostCode = "<br>" + PostCode.ToUpper();
            }

            string address = string.Format("{0}{1}{2}{3}{4}", Address1, Address2, Address3, Address4, PostCode.ToUpper());
            return address;
        }
    }
}
