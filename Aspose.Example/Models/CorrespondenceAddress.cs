﻿namespace Aspose.Example.Models
{
    public class CorrespondenceAddress
    {
        public int prhst_occ_num { get; set; }
        public string nam { get; set; }

        //[JsonProperty("addr##1")]
        public string addr1 { get; set; }

        //[JsonProperty("addr##2")]
        public string addr2 { get; set; }

        //[JsonProperty("addr##3")]
        public string addr3 { get; set; }

        //[JsonProperty("addr##4")]
        public string addr4 { get; set; }
        public string postcode { get; set; }
        public object end_dte { get; set; }

    }
}
