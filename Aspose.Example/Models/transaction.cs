﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aspose.Example.Models
{
    public class transaction
    {
       public int accountid { get; set; }
       public int? invoice_number { get; set; }

        public int subacct { get; set; }

        public DateTime? date  { get; set; }

        public string dsc { get; set; }

        public string longDsc { get; set; }

        public decimal val { get; set; }

        public decimal runningtotal { get; set; }

        public string tran_cde { get; set; }

        public string ref_no { get; set; }

        public List<charge> charges { get; set; }

        public int RowNum { get; set; }
    }
}
