﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aspose.Example.Models
{
    // TODO: Remove UI properties from API model
    public class summary
    {
        public int? acctID { get; set; }
        public int? subAcct { get; set; } = 0;
        public string fnam { get; set; }
        public string snam { get; set; }
        public string addr1 { get; set; }
        public int? pr_seq_no { get; set; }
        public int? per_num { get; set; }
        public DateTime? tncy_commence_date { get; set; }
        public DateTime? tncy_terme_date { get; set; }
        public DateTime? person_start_date { get; set; }
        public DateTime? person_end_date { get; set; }
        public string dsc { get; set; }
        public decimal? total { get; set; } = 0;
        // Below are UI properties?
        public string arrearsActionCode { get; set; }
        public string arrearsActionMessage { get; set; }
        public string arrearsActionIcon { get; set; }
        public string arrearsActionBackground { get; set; }
        public string icon { get; set; }
    }
}
