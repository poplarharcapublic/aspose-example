﻿using System;
using System.Collections.Generic;

namespace Aspose.Example.Models
{
    public class tenancyPerson
    {
        public int personid { get; set; }
        public string personame { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public DateTime? dateofbirth { get; set; }
        public bool? ContactPHBeforeVisting { get; set; }
        public List<contactInfo> contactinfos { get; set; } = new List<contactInfo>();
    }
}
