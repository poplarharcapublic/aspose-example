﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aspose.Example.Models
{
    public class charge
    {
        public int? pr_seq_no { get; set; }

        public int? prhst_occ_num { get; set; }

        public int? invoice_number { get; set; }

        public string lsc_cde { get; set; }

        public string invoice_dsc { get; set; }

        public bool? estimate_actual { get; set; }
        public string charge_dsc { get; set; }
        public decimal? item_tot { get; set; }
        public decimal? annual_est_cost_calc { get; set; }
        public decimal? annual_actual_cost { get; set; }
        public int? year_cde { get; set; }
    }
}