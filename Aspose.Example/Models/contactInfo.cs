﻿namespace Aspose.Example.Models
{
    public class contactInfo
    {
      public int? personid { get; set; }
      public string contacttype { get; set; }
      public string contactdetails { get; set; }
      public string icon { get; set; }
    }
}
